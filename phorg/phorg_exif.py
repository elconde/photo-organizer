"""Tools for getting EXIF data from a file"""
import time
import exifread
import logging
import phorg
import datetime
import os
import re

LOGGER = logging.getLogger(__name__)


def get_datetime(photo_file_full_name):
    with open(photo_file_full_name, 'rb') as photo_file:
        tags = exifread.process_file(photo_file, stop_tag='Image DateTime')
    image_date_time: exifread.classes.IfdTag = tags.get('Image DateTime')
    if not image_date_time:
        return get_datetime_from_file_name(photo_file_full_name)
    else:
        image_date_time_value = image_date_time.values
    try:
        strptime = time.strptime(image_date_time_value, '%Y:%m:%d %H:%M:%S')
    except ValueError:
        log_error(image_date_time_value, photo_file_full_name)
        return
    return datetime.datetime.fromtimestamp(time.mktime(strptime))


def log_error(date_time_value, full_name):
    LOGGER.error(
        '%s: Cannot determine time from "%s"', full_name, date_time_value
    )


def get_datetime_from_file_name(photo_file_full_name):
    """Guess the date from the file name"""
    base_name = os.path.basename(photo_file_full_name)
    match = re.match(
        r'(^[12][09][0-9][0-9])[^0-9]?'
        r'([01][0-9])[^0-9]?'
        r'([0123][0-9])[^0-9]?'
        r'([012][0-9])[^0-9]?'
        r'([0-6][0-9])[^0-9]?'
        r'([0-6][0-9])', base_name
    )
    if match:
        year, month, day, hour, minute, second = [
            int(g) for g in match.groups()
        ]
        return datetime.datetime(year, month, day, hour, minute, second)
    match = re.match(
        r'(^[12][09][0-9][0-9])[^0-9]?'
        r'([01][0-9])[^0-9]?'
        r'([0123][0-9])[^0-9]?', base_name
    )
    if match:
        year, month, day = [int(g) for g in match.groups()]
        LOGGER.debug(
            '%s: year=%d, month=%d, day=%d', base_name, year, month, day
        )
        if year > 2100:
            log_error(base_name, photo_file_full_name)
            return
        return datetime.datetime(year, month, day)
    match = re.match(r'[^0-9]*(2[0-9]*_[0-9]*)\.', base_name)
    if match:
        return datetime.datetime.fromtimestamp(
            time.mktime(time.strptime(match.group(1), '%Y%m%d_%H%M%S'))
        )
    search = re.search(
        r'([12][09][0-9][0-9]).?([01][0-9]).?([0123][0-9]).?'
        r'([012][0-9]).?([0-6][0-9]).?([0-6][0-9])', base_name
    )
    if search:
        year, month, day, hour, minute, second = [
            int(g) for g in search.groups()
        ]
        return datetime.datetime(year, month, day, hour, minute, second)
    match = re.match('[0-9]*\.[^.]*', base_name)
    if (
        match and not base_name.startswith('20') and
        not base_name.startswith('19')
    ):
        log_error(base_name, photo_file_full_name)
        return
    match = re.match(
        'VID-([12][09][01789][0-9][01][0-9][0-6][0-9])-0000[0-9]', base_name
    )
    if match:
        return datetime.datetime.fromtimestamp(
            time.mktime(time.strptime(match.group(1), '%Y%m%d'))
        )
    match = re.match(
        '([12][09][01789][0-9]).([01][0-9]).([0-6][0-9]) [^0-9]', base_name
    ) or re.match('(1?[0-9])-([0-3][0-9])-(9[0-9]) 0', base_name)
    if match:
        month, day, year = [int(g) for g in match.groups()]
        year = year + (1900 if year > 70 else 2000)
        LOGGER.debug(
            '%s> year: %d, month: %d, day: %d', base_name, year, month, day
        )
        return datetime.datetime(year, month, day)
    search = re.search(
        r'([12][09][0-9][0-9]).?([01][0-9]).?([0123][0-9])', base_name
    )
    if search:
        year, month, day = [int(g) for g in search.groups()]
        return datetime.datetime(year, month, day)
    # if (
    #     base_name.startswith('IMG') and len(base_name) < 24 or
    #     re.match(r'[^12][0-9]*\.[^.]*', base_name) or
    #     base_name.startswith('MOV') and len(base_name) < 11 or
    #     sum(c.isdigit() for c in base_name) < 6 or
    #     len(base_name) >= 11 and base_name.startswith('3') or
    #     re.match(
    #         '[a-f0-9]*-[a-f0-9]*-[a-f0-9]*-[a-f0-9]*-[a-f0-9]*', base_name,
    #         re.IGNORECASE
    #     ) or
    #     len(base_name) <= 12 and not (
    #         base_name.startswith('1') or base_name.startswith('2')
    #     )
    # ):
    #     log_error(base_name, photo_file_full_name)
    #     return
    log_error(base_name, photo_file_full_name)


if __name__ == '__main__':
    phorg.setup_logging(verbose=True)
    date_ = get_datetime(
        '/home/elconde/Flickr-Download/farm1/783/DSC01731.jpg'
    )
    LOGGER.debug('%s %s', date_, type(date_))

__all__ = ['get_datetime']
