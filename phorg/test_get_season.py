import unittest
import phorg
import logging
import datetime

LOGGER = logging.getLogger(__name__)


class TestGetSeason(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        phorg.setup_logging(verbose=True)

    def test_get_season(self):
        self.assertEqual(
            '2019-1-winter', phorg.get_season(datetime.datetime(2019, 1, 1))
        )
        self.assertEqual(
            '1991-1-winter', phorg.get_season(datetime.datetime(1990, 12, 25))
        )
        self.assertEqual(
            '1989-3-summer', phorg.get_season(datetime.datetime(1989, 7, 4))
        )
        self.assertEqual(
            '1985-2-spring', phorg.get_season(
                datetime.datetime(1985, 3, 20, 11, 13)
            )
        )
        self.assertEqual(
            '1985-1-winter', phorg.get_season(
                datetime.datetime(1985, 3, 20, 11, 12)
            )
        )
