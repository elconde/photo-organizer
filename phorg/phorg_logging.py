"""Logger. To log a message do:

>>> import phorg
>>> import logging
>>>
>>> phorg.setup_logging()
>>> LOGGER = logging.getLogger(__name__)
>>>
>>> LOGGER.info('hello world')"""

import logging


def setup_logging(verbose=False):
    """Set up the root logger. This only needs to be run once. Also
    disable the exifread logger."""
    logging.basicConfig(
        format="%(asctime)s %(levelname).1s %(name)-23s %(message)s",
        level=logging.DEBUG if verbose else logging.INFO,
        datefmt='%Y-%m-%d %H:%M:%S',
    )
    exif_logger = logging.getLogger('exifread')
    exif_logger.disabled = True


__all__ = ['setup_logging']
