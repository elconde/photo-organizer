#!/usr/bin/env python
"""Organizes photos into directories and removes duplicates."""
import signal
import argparse
import hashlib
import shutil
import sys
import phorg
import logging
import os
import time
import bidict
import pickle

LOGGER = logging.getLogger(__name__)
MD5_SUMS = None
MD5_SUMS_FILE_NAME = '.md5_sums'


def parse_args():
    """Parse the command line arguments"""
    parser = argparse.ArgumentParser(description=__doc__.splitlines()[0])

    def directory(string_argument):
        """Expand user"""
        return os.path.expanduser(string_argument)
    default_ = ' (default: %(default)s)'
    parser.add_argument(
        'input_dir', help='directory where the photos live.'+default_,
        type=directory
    )
    parser.add_argument(
        '--output-dir',
        help='directory where the organized photos will go.'+default_,
        default='.', type=directory
    )
    parser.add_argument(
        '--verbose', help='Verbose logging', action='store_true'
    )
    parser.add_argument(
        '--dry-run', help='Just print what would be done', action='store_true'
    )
    parser.add_argument(
        '--log-duplicates', help='Log duplicate files', action='store_true'
    )
    parser.add_argument(
        '--log-copies', help='Log each file that gets copied',
        action='store_true'
    )
    return parser.parse_args()


def get_full_names(directory):
    """Get all the full file names for every file in this directory,
    recursively."""
    result = []
    for dirpath, dirnames, filenames in os.walk(directory):
        for file_name in filenames:
            if file_name == MD5_SUMS_FILE_NAME:
                continue
            full_name = os.path.join(dirpath, file_name)
            result.append(full_name)
    return sorted(result)


def initialize_md5_sums(directory, total):
    """Get the MD5 sums of all files in the output directory"""
    global MD5_SUMS
    abs_path = os.path.abspath(directory)

    LOGGER.info('%s: Initializing MD5 sums', os.path.abspath(abs_path))

    def set_md5_sum(full_name):
        if full_name in MD5_SUMS:
            return False
        md5_sum = get_md5_sum(full_name)
        assert md5_sum not in MD5_SUMS.inverse, (
            'Duplicate out files: {} = {}'.format(
                full_name, MD5_SUMS.inverse[md5_sum]
            )
        )
        MD5_SUMS[full_name] = md5_sum
        return True

    do_while_logging_count(
        set_md5_sum, abs_path,
        '%(processed)d out of {} files summed '
        '(%(skipped)d files skipped)'.format(total)
    )


def do_while_logging_count(func, directory, log_message, frequency=3):
    """Perform this function over all the files in this directory
    (recursively) while logging the count. The logging frequency is in
    seconds. Don't log over that frequency. Use the _processed_,
    _total_, and _skipped_ keywords in the log message."""
    last_logged_time = time.time()
    processed = 0
    skipped = 0
    full_names = get_full_names(directory)
    total = len(full_names)
    for full_name in full_names:
        if func(full_name):
            processed += 1
        else:
            skipped += 1
        if time.time() - last_logged_time < frequency:
            continue
        LOGGER.info(
            log_message,
            {'processed': processed, 'skipped': skipped, 'total': total}
        )
        last_logged_time = time.time()
    LOGGER.info(
        log_message,
        {'processed': processed, 'skipped': skipped, 'total': total}
    )


def photo_organizer(args):
    """Recurse through the photos copying them to the output
    directory."""
    LOGGER.info('Copying files from %s to %s', args.input_dir, args.output_dir)

    def copy_to_output_dir(full_name):
        return copy_photo(full_name, args)

    do_while_logging_count(
        copy_to_output_dir, args.input_dir,
        'Copied %(processed)d files of %(total)d (%(skipped)d skipped)'
    )


def copy_photo(full_name, args):
    """Copy a photo to the output directory. Return True if copied."""
    md5_sum = get_md5_sum(full_name)
    if md5_sum in MD5_SUMS.inverse:
        if not args.log_duplicates:
            return False
        LOGGER.warning(
            '%s: Duplicate with %s', full_name, MD5_SUMS.inverse[md5_sum]
        )
        return False
    date_dir_name = get_date_dir_name(full_name)
    dest_dir = os.path.abspath(os.path.join(args.output_dir, date_dir_name))
    if not os.path.isdir(dest_dir):
        LOGGER.info('Creating directory: %s', dest_dir)
        if not args.dry_run:
            os.makedirs(dest_dir)
    dest = os.path.join(dest_dir, os.path.basename(full_name))
    if args.log_copies:
        LOGGER.info('%s => %s', full_name, dest)
    if args.dry_run:
        return True
    shutil.copy(full_name, dest)
    MD5_SUMS[dest] = md5_sum
    return True


def get_date_dir_name(full_name):
    """What should the date directory name be?"""
    photo_datetime = phorg.get_datetime(full_name)
    if not photo_datetime or photo_datetime.year < 1900:
        return 'None'
    return phorg.get_season(photo_datetime)


def get_md5_sum(full_name):
    """Get the MD5 sum of a file"""
    with open(full_name, 'rb') as f_open:
        return hashlib.md5(f_open.read()).hexdigest()


def load_cached_md5_sums():
    """Load up the MD5 sums from the cache files"""
    global MD5_SUMS
    if not os.path.exists(MD5_SUMS_FILE_NAME):
        MD5_SUMS = bidict.bidict()
        return
    with open(MD5_SUMS_FILE_NAME, 'rb') as md5_sums_file:
        MD5_SUMS = pickle.load(md5_sums_file)
    return


# noinspection PyUnusedLocal
def sigint_handler(signum, frame):
    save_md5_sums()
    sys.exit(1)


def save_md5_sums():
    LOGGER.info('Saving MD5 sums to %s', MD5_SUMS_FILE_NAME)
    with open(MD5_SUMS_FILE_NAME, 'wb') as md5_out_file:
        pickle.dump(MD5_SUMS, md5_out_file)


def main():
    """Parse the command line arguments, load the cached output MD5
    sums, initialize the remaining output MD5 sums, and organize the
    photos."""
    args = parse_args()
    signal.signal(signal.SIGINT, sigint_handler)
    phorg.setup_logging(args.verbose)
    if args.dry_run:
        LOGGER.warning('Dry run: Nothing will be saved')
    load_cached_md5_sums()
    output_dir = os.path.abspath(args.output_dir)
    total_out = len(list(get_full_names(output_dir)))
    initialize_md5_sums(output_dir, total_out)
    photo_organizer(args)
    save_md5_sums()



if __name__ == '__main__':
    main()
